# Object Detection 
Pytorch implementation of real time object detection algorithm YOLOv3


To run this detection algorithm, after cloning the repo, download pretrained weights: 
```
wget https://pjreddie.com/media/files/yolov3.weights
```

## Requirements

- Python 3.X
- Pytorch >= 1.0
- OpenCV 3.4

## Usage

```
usage: detector.py [-h] -i INPUT [-t OBJ_THRESH] [-n NMS_THRESH] [-o OUTDIR]
                   [-v] [-w] [--cuda] [--no-show]

YOLOv3 object detection

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        input image or directory or video
  -t OBJ_THRESH, --obj-thresh OBJ_THRESH
                        objectness threshold, DEFAULT: 0.5
  -n NMS_THRESH, --nms-thresh NMS_THRESH
                        non max suppression threshold, DEFAULT: 0.4
  -o OUTDIR, --outdir OUTDIR
                        output directory, DEFAULT: detection/
  -v, --video           flag for detecting a video input
  -w, --webcam          flag for detecting from webcam. Specify webcam ID in
                        the input. usually 0 for a single webcam connected
  --cuda                flag for running on GPU
  --no-show             do not show the detected video in real time
```


#### Detecting on a image or a directory containing images

`python3 detector.py -i <input>`

Result will be save in \<outdir\>


demo

![demo](detection/det_vma.png)

![demo](detection/det_cats.jpg)

#### Detecting on a video or a webcam

- Video `python3 detector.py -i <input> -v`
- Webcam `python3 detector.py -v -w -i 0`

This will do object detection on the video or webcam(0 is the webcam ID. Change it if you have multiple webcam connected) and show the result in real time and save the detected video with bounding boxes flying around(without audio).


